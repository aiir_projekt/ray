#!/usr/bin/python3
import time

import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='queue')

channel.basic_publish(exchange='',
                      routing_key='queue',
                      body=bytes(f'{{"id": "500", "min_x": "-1", "max_x": "1", "min_y": "-1", "max_y": "1", "w": "10", "h": "10", "type": "julia", "iters": "100"}}', 'ascii'))

channel.queue_declare(str(500))
for method, properties, body in channel.consume(str(500)):
    channel.basic_ack(method.delivery_tag)
    channel.cancel()

fractal_id = 1000
size = [[1280, 720], [1920, 1080], [2560, 1440], [3840, 2160]]

for i in range(4):
    for j in range(5):
        message = f'{{"id": "{fractal_id}", "min_x": "-1", "max_x": "1", "min_y": "-1", "max_y": "1", "w": "{size[i][0]}", "h": "{size[i][1]}", "type": "julia", "iters": "100"}}'

        start = time.time()

        channel.basic_publish(exchange='',
                              routing_key='queue',
                              body=bytes(message, 'ascii'))

        channel.queue_declare(str(fractal_id))
        for method, properties, body in channel.consume(str(fractal_id)):
            channel.basic_ack(method.delivery_tag)
            print(time.time() - start)
            channel.cancel()

        fractal_id += 1

for i in range(4):
    for j in range(5):
        message = f'{{"id": "{fractal_id}", "min_x": "-1", "max_x": "1", "min_y": "-1", "max_y": "1", "w": "{size[i][0]}", "h": "{size[i][1]}", "type": "mandel", "iters": "100"}}'

        start = time.time()

        channel.basic_publish(exchange='',
                              routing_key='queue',
                              body=bytes(message, 'ascii'))

        channel.queue_declare(str(fractal_id))
        for method, properties, body in channel.consume(str(fractal_id)):
            channel.basic_ack(method.delivery_tag)
            print(time.time() - start)
            channel.cancel()

        fractal_id += 1

connection.close()
