FROM rayproject/ray:latest

RUN python3 -m venv venv

#install requirements
COPY requirements.txt .
RUN . venv/bin/activate && pip install --upgrade pip && pip install -r requirements.txt

COPY . .
EXPOSE 8889
EXPOSE 8888
CMD . venv/bin/activate && exec python3 main.py
