#!/usr/bin/python3

import base64
import json
import os
import sys
import ray

import pika
from PIL import Image

from Fractal import *
from io import BytesIO


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue='queue')
    channel.basic_consume(queue='queue',
                          auto_ack=True,
                          on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


def callback(ch, method, properties, body):
    body = json.loads(body.decode())
    print(f'GOT {body}')
    fractal = Fractal(
        body['id'],
        int(body['min_x']),
        int(body['max_x']),
        int(body['min_y']),
        int(body['max_y']),
        int(body['w']),
        int(body['h']),
        body['type'],
        int(body['iters'])
    )

    if fractal.func == 'julia':
        matrix = fractal.create_julia()
    elif fractal.func == 'mandel':
        matrix = fractal.create_mandel()
    else:
        matrix = None

    try:
        data = Image.fromarray(matrix)
        if data.mode != 'RGBA':
            data = data.convert('RGBA')
        buffered = BytesIO()
        data.save(buffered, format="PNG")
        img_str = str(base64.b64encode(buffered.getvalue()), 'utf-8')

        print(f'DONE {body["id"]}')

        ch.basic_publish(exchange='',
                        routing_key=str(body["id"]),
                         body='{"image":"data:image/png;base64,' + img_str + '"}')
    except Exception as e:
        print(str(e))
        ch.basic_publish(exchange='',
                         routing_key=str(body["id"]),
                         body='')


if __name__ == "__main__":
    os.system('ray start --head --port 8888 --dashboard-host 0.0.0.0 --dashboard-port 8889')
    ray.init(address='auto', _redis_password='5241590000000000')
    try:
        main()
    except Exception as e:
        main()
