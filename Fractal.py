import numpy as np
import ray


def julia(x, y, max_iters):
    z = complex(x,y)
    c = -0.7 + 0.27015j

    for i in range(max_iters):
        z = z*z + c
        if (z.real*z.real + z.imag*z.imag) >= 2:
            return i

    return max_iters


def mandel(x, y, max_iters):
    c = complex(x, y)
    z = 0.0j

    for i in range(max_iters):
        z = z*z + c
        if (z.real*z.real + z.imag*z.imag) >= 4:
            return i
    return max_iters


class Fractal:
    def __init__(self, id, min_x, max_x, min_y, max_y, w, h, func, iters):
        self.id = id
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.w = w
        self.h = h
        self.func = func
        self.iters = iters

    def create_julia(self):
        pixel_size_x = (self.max_x - self.min_x) / self.w
        pixel_size_y = (self.max_y - self.min_y) / self.h

        return np.array(ray.get([self.calculate_line.remote(self, x, pixel_size_x, pixel_size_y, julia) for x in range(self.w)])).transpose()

    def create_mandel(self):
        pixel_size_x = (self.max_x - self.min_x) / self.w
        pixel_size_y = (self.max_y - self.min_y) / self.h

        return np.array(ray.get([self.calculate_line.remote(self, x, pixel_size_x, pixel_size_y, mandel) for x in range(self.w)])).transpose()

    @ray.remote
    def calculate_line(self, x, pixel_size_x, pixel_size_y, func):
        line = np.zeros(self.h)
        real = self.min_x + x * pixel_size_x
        for y in range(self.h):
            imag = self.min_y + y * pixel_size_y
            color = func(real, imag, self.iters)
            line[y] = color
        return line
