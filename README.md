**Usage**

Create and run python virtual environment

`python3 -m venv venv`

`source venv/bin/activate`

Install requirments using command below

`pip3 install -r requirements.txt `

Start server from fractal/app catalog

`./main.py`
or
`python3 main.py`

Before, run RabbitMQ docker

`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management`
